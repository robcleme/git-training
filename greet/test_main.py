import pytest
import sys

from pathlib import Path
from .main import main

def test_main_no_args():
    with pytest.raises(TypeError):
        main()

def test_main_positional_args(capsys):
    main("Bob")
    out, _err = capsys.readouterr()
    assert out == "Hello Bob !"

def test_main_keyword_args(capsys):
    main(name="Alice")
    out, _err = capsys.readouterr()
    assert out == "Hello Alice !"
