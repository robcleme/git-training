#!/usr/bin/env pyhton3
import argparse
import os
from .main import main

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--name", default=os.getenv("USER"), help="person to greet")
    args = parser.parse_args()

    ret = main(name=args.name)
    exit(ret)
